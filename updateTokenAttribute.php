<?php
/**
 * updateTokenAttribute Plugin for LimeSurvey
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2014 Denis Chenu <http://sondages.pro>
 * @license AGPL v3
 * @version 1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 */
class updateTokenAttribute extends PluginBase
{
    protected $storage = 'DbStorage';

    static protected $name = 'updateTokenAttribute';
    static protected $description = 'Update the token attribute with the DB at each survey page';

    protected $settings = array(
        'active'=>array(
            'type'=>'boolean',
            'label'=>'Activate update token attribute by default on all survey with token (not anonymous)',
            'default'=>0,
        ),
    );
    public function __construct(PluginManager $manager, $id) {
        parent::__construct($manager, $id);
        $this->subscribe('beforeSurveyPage');
        $this->subscribe('beforeSurveySettings');
        $this->subscribe('newSurveySettings');
    }

    public function beforeSurveySettings()
    {
        $event = $this->event;
        $event->set("surveysettings.{$this->id}", array(
            'name' => get_class($this),
            'settings' => array(
                'active'=>array(
                    'type'=>'boolean',
                    'label'=>'Activate update token attribute by default on all survey with token (not anonymous)',
                    'current'=>$this->get('active', 'Survey', $event->get('survey'),$this->get('active')),
                ),
            )
        ));
    }

    public function newSurveySettings()
    {
        $event = $this->event;
        foreach ($event->get('settings') as $name => $value)
        {
            /* In order use survey setting, if not set, use global, if not set use default */
            $default=$event->get($name,null,null,isset($this->settings[$name]['default'])?$this->settings[$name]['default']:NULL);
            $this->set($name, $value, 'Survey', $event->get('survey'),$default);
        }
    }

    public function beforeSurveyPage()
    {
        $oEvent=$this->getEvent();
        $iSurveyId=$oEvent->get('surveyId');

        if(!$this->get('active', 'Survey', $iSurveyId,$this->get('active')))
            return;
        $oSurvey=Survey::model()->findByPk($iSurveyId);
        if($oSurvey && $oSurvey->anonymized=="N" && $oSurvey->tokenanswerspersistence=="Y")
        {
            if(Yii::app()->db->schema->getTable('{{tokens_'.$iSurveyId.'}}'))
            {
                $sessionSurvey=Yii::app()->session["survey_{$iSurveyId}"];
                $sToken=!empty($sessionSurvey['token']) ? $sessionSurvey['token'] : App()->getRequest()->getParam('token');
                $sessionTokenAttribute=Yii::app()->session["updateTokenAttribute"];
                if($sToken)
                {
                    if(!isset($sessionTokenAttribute[$iSurveyId][$sToken]))
                    {
                        $oToken=TokenDynamic::model($iSurveyId)->find("token=:token",array(":token"=>$sToken));
                        if($oToken)
                            $sessionTokenAttribute[$iSurveyId][$sToken]=$oToken->attributes;
                        else
                            $sessionTokenAttribute[$iSurveyId][$sToken]=array();
                        Yii::app()->session["updateTokenAttribute"]=$sessionTokenAttribute;
                    }
                    elseif(isset($sessionSurvey['srid'])){
                        $oActualResponse=SurveyDynamic::model($iSurveyId)->find("id=:srid",array(":srid"=>$sessionSurvey['srid']));
                        if($oActualResponse)
                        {
                            $oToken=TokenDynamic::model($iSurveyId)->find("token=:token",array(":token"=>$sToken));
                            $aOldTokenAttribute=$sessionTokenAttribute[$iSurveyId][$sToken];                        
                            if($oToken)
                            {
                                $bTokenUpdated=false;
                                foreach($oToken->attributes as $attribute=>$value)
                                {
                                    if((!isset($aOldTokenAttribute[$attribute]) && !is_null($value)) || $aOldTokenAttribute[$attribute]!=$value)
                                    {
                                        $bTokenUpdated=true;
                                    }
                                }
                                //$bTokenUpdated=true;
                                if($bTokenUpdated)
                                {
                                    $sessionTokenAttribute[$iSurveyId][$sToken]=$oToken->attributes;
                                    Yii::app()->session["updateTokenAttribute"]=$sessionTokenAttribute;
                                    $actualStep=$sessionSurvey['step'];
                                    $sLanguage=$sessionSurvey['s_lang'];

                                    $clienttoken=$sToken;
                                    $thissurvey=getSurveyInfo($iSurveyId,$sLanguage);
                                    switch ($thissurvey['format'])
                                    {
                                        case "A": //All in one
                                            $surveyMode = 'survey';
                                            break;
                                        default:
                                        case "S": //One at a time
                                            $surveyMode = 'question';
                                            break;
                                        case "G": //Group at a time
                                            $surveyMode = 'group';
                                            break;
                                    }
                                    $radix=getRadixPointData($thissurvey['surveyls_numberformat']);
                                    $radix = $radix['separator'];
                                    $surveyOptions = array(
                                        'active' => ($thissurvey['active'] == 'Y'),
                                        'allowsave' => ($thissurvey['allowsave'] == 'Y'),
                                        'anonymized' => ($thissurvey['anonymized'] != 'N'),
                                        'assessments' => ($thissurvey['assessments'] == 'Y'),
                                        'datestamp' => ($thissurvey['datestamp'] == 'Y'),
                                        'deletenonvalues'=>Yii::app()->getConfig('deletenonvalues'),
                                        'hyperlinkSyntaxHighlighting' => false, // TODO set this to true if in admin mode but not if running a survey
                                        'ipaddr' => ($thissurvey['ipaddr'] == 'Y'),
                                        'radix'=>$radix,
                                        'refurl' => (($thissurvey['refurl'] == "Y" && isset($sessionSurvey['refurl'])) ? $sessionSurvey['refurl'] : NULL),
                                        'savetimings' => ($thissurvey['savetimings'] == "Y"),
                                        'surveyls_dateformat' => (isset($thissurvey['surveyls_dateformat']) ? $thissurvey['surveyls_dateformat'] : 1),
                                        'startlanguage'=>(isset($clang->langcode) ? $clang->langcode : $thissurvey['language']),
                                        'target' => Yii::app()->getConfig('uploaddir').DIRECTORY_SEPARATOR.'surveys'.DIRECTORY_SEPARATOR.$thissurvey['sid'].DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR,
                                        'tempdir' => Yii::app()->getConfig('tempdir').DIRECTORY_SEPARATOR,
                                        'timeadjust' => Yii::app()->getConfig("timeadjust"),
                                        'token' => (isset($clienttoken) ? $clienttoken : NULL),
                                    );
                                    Yii::app()->session['LEMdirtyFlag']=true;

                                    LimeExpressionManager::StartSurvey($iSurveyId, $surveyMode, $surveyOptions, false);
                                    $_SESSION['survey_'.$iSurveyId]['step']=$actualStep;
                                    //~ if(isset($oActualResponse->datestamp))
                                        //~ $_SESSION['survey_'.$iSurveyId]['datestamp']=$oActualResponse->datestamp;
                                    if(isset($oActualResponse->startdate))
                                        $_SESSION['survey_'.$iSurveyId]['startdate']=$oActualResponse->startdate;
                                    if(isset($oActualResponse->refurl))
                                        $_SESSION['survey_'.$iSurveyId]['refurl']=$oActualResponse->refurl;
                                    if(isset($oActualResponse->lastpage) && $oActualResponse->lastpage>$actualStep)
                                    {
                                            $_SESSION['survey_'.$surveyid]['maxstep']=$oActualResponse->lastpage ;
                                    }
                                    //~ tracevar($oActualResponse->datestamp);
                                    LimeExpressionManager::JumpTo($actualStep, false, false,true);
                                    tracevar("done");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
